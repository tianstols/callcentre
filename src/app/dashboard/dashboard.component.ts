import { Component, OnInit, ViewChild } from '@angular/core';
import { IncomingcallComponent } from '../incomingcall/incomingcall.component';
import {Command} from '../commands';
import { SocketService } from '../socket.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private socketService: SocketService) { }

  ngOnInit() {
    //this.setupPoolConnection("localhost:9090", Math.random().toString());
    this.socketService.setContext(this)
  }

  @ViewChild("incomingCall") incomingCall: IncomingcallComponent;
  //private socket: WebSocket;
  

  public connect(ID: string){
    //this.setupPoolConnection("172.23.0.2:9091",ID);
  }

 /*  private setupPoolConnection(addr: string, clientID: string): WebSocket{
    this.socket = new WebSocket("ws://"+addr+"/pool/"+clientID);
    this.socket.onopen = (evt) => this.socketOpen(evt);
    this.socket.onclose = (evt) => this.socketClose(evt);
    this.socket.onerror = (evt) => this.socketError(evt);
    this.socket.onmessage = (evt) => this.socketMessage(evt);
    return this.socket;
  } */

  private socketOpen(evt: Event){
    console.log("Websocket open")
    /* let cmd = new Command("next")
    let json = JSON.stringify(cmd)
    this.socket.send(json) */
  }

  private socketClose(evt: CloseEvent){
    console.log("Websocket close")
    console.log(evt)
  }

  private socketError(evt: Event){
    console.log("Websocket error")
    console.log(evt)
  }

  private async socketMessage(evt: MessageEvent){
    //console.log(evt.data)
    let msg = JSON.parse(evt.data);
    console.log(msg)
    switch (msg.type){
      case "call":
      console.log("Incoming call")
      //this.incomingCall.open(this.socket,msg.data);
        break;
      case "answered":
      console.log("Answered")
        this.incomingCall.close();
        break;
    }
  }
  
}
