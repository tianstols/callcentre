import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor() {
    this.state = "available";
   }

  private socket: WebSocket;
  private state: string;

  public setupPoolConnection(addr: string, queue: string,clientID: string, context: any): WebSocket{
    let token = btoa(queue+":"+clientID)
    this.socket = new WebSocket("ws://"+addr+"/pool/"+token);
    this.socket.onopen = (evt) => this.socketOpen(evt);
    this.socket.onclose = (evt) => this.socketClose(evt);
    this.socket.onerror = (evt) => this.socketError(evt);
    this.socket.onmessage = (evt) => this.socketMessage(evt,context);
    return this.socket;
  }

  public setContext( context: any) {
    this.socket.onmessage = (evt) => this.socketMessage(evt,context);
  }

  public getSocket(): WebSocket {
    return this.socket;
  }

  public getState(): string {
    return this.state;
  }

  public send(msg: string) {
    this.socket.send(msg)
  }

  public close(){
    this.socket.close()
  }

  private socketOpen(evt: Event){
    console.log("Websocket open")
  }

  private socketClose(evt: CloseEvent){
    console.log("Websocket close")
    console.log(evt)
  }

  private socketError(evt: Event){
    console.log("Websocket error")
    console.log(evt)
  }

  private async socketMessage(evt: MessageEvent,ctx: any){
    let msg = JSON.parse(evt.data);
    //console.log(msg)
    switch (msg.type){
      case "call":
      console.log("THIS IS A CALL")
      ctx.incomingCall.open(msg.data as string);
        break;
      case "answered":
        ctx.incomingCall.close();
        break;
        case "config":
        console.log("Received config", msg);
        let config = msg.data;//JSON.parse(msg.data) as RTCConfiguration;
        ctx.setupRTC(config)
        break;
        case "sendoffer":
          await ctx.createLocalOffer(ctx.offerOptions);
          break;
        case "offer":
          console.log("Received offer");

          let offer = msg.data as RTCSessionDescriptionInit;//JSON.parse(msg.data) as RTCSessionDescriptionInit;
          await ctx.localClient.setRemoteDescription(offer);
          await ctx.createLocalAnswer();
          break;
        case "answer":
          console.log("Received answer");
          let answer = msg.data as RTCSessionDescriptionInit;//JSON.parse(msg.data) as RTCSessionDescriptionInit;
          await ctx.localClient.setRemoteDescription(answer);
          break;
        case "closing":
        ctx.socketService.close()
          //close webRTC stuff
          ctx.remoteVideo.nativeElement.srcObject = null;
          ctx.remoteVideoChange.emit(ctx.remoteVideo);
          //joinRoom();
          break;
        case "icecandidate":
        //new Promise(resolve => setTimeout(resolve,1000))
          console.log("Received ICE candidate");
          let iceCandidate = msg.data as RTCIceCandidateInit;//JSON.parse(msg.data) as RTCIceCandidateInit;
          await ctx.localClient.addIceCandidate(iceCandidate);
          break;
          case "ping":
          let data = JSON.stringify({type: "pong", data: "pong"})
          ctx.socketService.send(data)
          break;
          case "stateupdate":
          console.log("This is a state update", msg.data)
          this.state = msg.data;
          break;
        default:
          console.log("Unknown message");
          console.log(msg);

    }
  }
}
