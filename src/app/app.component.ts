import { Component, Input, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { IncomingcallComponent } from 'src/app/incomingcall/incomingcall.component';
import {SocketService} from 'src/app/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'callcentre';
  public state: string;
  constructor(private socketService: SocketService){}
  ngOnInit() {
    this.state = "available"
    this.socketService.setupPoolConnection("34.244.50.45:9095", "76","Agent", this);
  }

  public switchState(){
    this.state = this.socketService.getState();
    if (this.state == "available") {
      this.state = "busy";
      let data = JSON.stringify({type:"stateupdate", data: "busy"})
      this.socketService.send(data)
    } else if (this.state == "busy") {
    this.state = "available";
    let data = JSON.stringify({type:"stateupdate", data: "available"})
      this.socketService.send(data)
  } else if (this.state == "incall") {
    this.state = "available";
    let data = JSON.stringify({type:"stateupdate", data: "available"})
      this.socketService.send(data)
  }
}

public sendAck(){
  let data = JSON.stringify({type:"ack", data: "testingACK"})
      this.socketService.send(data)
}

  /* private setupPoolConnection(addr: string, clientID: string): WebSocket{
    this.socket = new WebSocket("ws://"+addr+"/pool/"+clientID);
    this.socket.onopen = (evt) => this.socketOpen(evt);
    this.socket.onclose = (evt) => this.socketClose(evt);
    this.socket.onerror = (evt) => this.socketError(evt);
    this.socket.onmessage = (evt) => this.socketMessage(evt);
    return this.socket;
  } */

/*   private socketOpen(evt: Event){
    console.log("Websocket open")

  }

  private socketClose(evt: CloseEvent){
    console.log("Websocket close")
    console.log(evt)
  }

  private socketError(evt: Event){
    console.log("Websocket error")
    console.log(evt)
  }*/


  /* private socketMessage(evt: MessageEvent){
    console.log("THIS IS A MESSAGE THROUGH PUBSUB");
    console.log(evt);
  } */

}
