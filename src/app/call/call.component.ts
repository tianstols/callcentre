import { Component, OnInit, ViewChild } from '@angular/core';
import {WebrtcService} from '../webrtc.service'
import { ActivatedRoute } from '@angular/router';
import { SocketService } from '../socket.service';
import {Available} from '../message-functions'
//import {WebrtcService} from '../webrtc.service'

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.css']
})
export class CallComponent implements OnInit {

  public webRTC: WebrtcService;
  public state : string;

  constructor(private route: ActivatedRoute, webRTCService: WebrtcService, private socketService: SocketService) {this.webRTC = webRTCService; }

  
  @ViewChild("localVideo") public localVideo: any;
  @ViewChild("remoteVideo") public remoteVideo: any;
  @ViewChild("videocontainer") public videocontainer: any;

  ngOnInit() {
    this.state = "busy";
    this.webRTC.localVideoChange.subscribe(video => {this.localVideo = video;})
    this.webRTC.remoteVideoChange.subscribe(video => {this.remoteVideo = video;})
    this.connectToRoom();
  }

  public async connectToRoom(){
    console.log("CALLING")
    let roomID = this.route.snapshot.paramMap.get("roomID")
    this.webRTC.setupRoomConnection(roomID,this.localVideo,this.remoteVideo);
    //Hide call window and go open video, results and forms.
    console.log(roomID)
    this.videocontainer.nativeElement.hidden = false;
  }

  decline(){
  }

  public endcall(){
    let data = JSON.stringify({type: "hangup", data: "hangup"})
    this.webRTC.HangUp(data)
  }

  public toggleVideo(){
    this.webRTC.toggleVideo();
  }

  public changeState(){
    if (this.state == "busy"){
      this.state = "available"
      let data = JSON.stringify({type: "stateupdate", data: "available"})
      this.socketService.send(data)
    } else if (this.state == "available") {
      this.state = "busy"
      let data = JSON.stringify({type: "stateupdate", data: "busy"})
      this.socketService.send(data)
    }
  }

}

