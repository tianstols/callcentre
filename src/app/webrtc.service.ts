import { Injectable, Output, EventEmitter } from '@angular/core';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class WebrtcService {

  private config;

  public localClient: RTCPeerConnection;
  public localStream: MediaStream;
  public localVideo: any;
  public remoteVideo: any;

  constructor(private socketService: SocketService) { }

  @Output() localVideoChange: EventEmitter<any> = new EventEmitter();
  @Output() remoteVideoChange: EventEmitter<any> = new EventEmitter();

  private offerOptions: RTCOfferOptions;

  public async setupRTC(config: RTCConfiguration){
    this.offerOptions = {
      offerToReceiveAudio: true,
      offerToReceiveVideo: true
    };

    this.localClient = new RTCPeerConnection(config);

     try {
      this.localStream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});
      this.localVideo.nativeElement.srcObject = this.localStream;
      this.localVideoChange.emit(this.localVideo);
    } catch (e) {
      alert(e)
    }

    this.localStream.getTracks().forEach(track => this.localClient.addTrack(track, this.localStream));
    this.toggleVideo();
    this.localClient.addEventListener('icecandidate', e => this.onIceCandidate(e));
    this.localClient.addEventListener('iceconnectionstatechange', e => this.onIceStateChange(e));
    this.localClient.addEventListener('track', e => this.gotRemoteStream(e));
    //localClient.addEventListener('negotiationneeded',renegotiate)

  }

  private async onIceCandidate(event: RTCPeerConnectionIceEvent) {
    try {
        if (event.candidate !== null && event.candidate !== undefined){
            var data = JSON.stringify({type: "icecandidate", data: event.candidate})
            console.log("Sending ICE candidate")
            //console.log(event.candidate)
            await this.socketService.send(data)
        }
    } catch (e) {
      console.log(e)
    }
  }

  private onIceStateChange(event: Event) {
    if (this.localClient) {
      console.log(`ICE state: ${this.localClient.iceConnectionState}`);
      console.log('ICE state change event: ', event);
    }
  }

   private gotRemoteStream(e: RTCTrackEvent) {
    if (this.remoteVideo.nativeElement.srcObject !== e.streams[0]) {
      this.remoteVideo.nativeElement.srcObject = e.streams[0];
      this.remoteVideoChange.emit(this.remoteVideo);
    }
}

public async createLocalOffer(options: RTCOfferOptions){
  try {
    console.log('Creating offer');
    const offer = await this.localClient.createOffer(options);
    console.log('Setting local description');
    try {
      await this.localClient.setLocalDescription(offer);
    } catch (e) {
        console.log(`Failed to set session description: ${e.toString()}`);
    }
    console.log("Sending offer to signaling server")
    await this.sendRTCSessionDescription("offer",offer)
  } catch (e) {
    console.log(`Failed to create session description: ${e.toString()}`);
  }
  }

  public async createLocalAnswer(){
    var answer = await this.localClient.createAnswer();

    try {
      await this.localClient.setLocalDescription(answer);

    } catch (e) {
      console.log(e);
    }
    console.log("Sending answer to signaling server")
    this.sendRTCSessionDescription("answer",answer);
  }

  private sendRTCSessionDescription(type: string,sessionDesc: RTCSessionDescriptionInit){
    var data = JSON.stringify({type: type, data: sessionDesc})
    this.socketService.send(data)
  }

  public toggleVideo(){
    var videoTracks = this.localStream.getVideoTracks();
    if (videoTracks.length === 0) {
        console.log("No local video available.");
        return;
      }
    for (var i = 0; i < videoTracks.length; ++i) {
        videoTracks[i].enabled = !videoTracks[i].enabled;
      }
  }

  public toggleAudio(){
      var audioTracks = this.localStream.getAudioTracks();
      if (audioTracks.length === 0) {
          console.log("No local audio available.");
          return;
        }
        for (var i = 0; i < audioTracks.length; ++i) {
          audioTracks[i].enabled = !audioTracks[i].enabled;
        }
  }
  private async socketMessage(evt: MessageEvent,ctx: any){
    console.log("Websocket message")
    //console.log(evt.data)
    var msg = JSON.parse(evt.data);

      switch (msg.type){
        case "config":
        console.log("Received config");
        let config = msg.data;//JSON.parse(msg.data) as RTCConfiguration;
        ctx.setupRTC(config)
        break;
        case "sendoffer":
          await ctx.createLocalOffer(ctx.offerOptions);
          break;
        case "offer":
          console.log("Received offer");

          let offer = msg.data as RTCSessionDescriptionInit;//JSON.parse(msg.data) as RTCSessionDescriptionInit;
          await ctx.localClient.setRemoteDescription(offer);
          await ctx.createLocalAnswer();
          break;
        case "answer":
          console.log("Received answer");
          let answer = msg.data as RTCSessionDescriptionInit;//JSON.parse(msg.data) as RTCSessionDescriptionInit;
          await ctx.localClient.setRemoteDescription(answer);
          break;
        case "closing":
        ctx.socketService.close()
          //close webRTC stuff
          ctx.remoteVideo.nativeElement.srcObject = null;
          ctx.remoteVideoChange.emit(ctx.remoteVideo);
          //joinRoom();
          break;
        case "icecandidate":
        //new Promise(resolve => setTimeout(resolve,1000))
          console.log("Received ICE candidate");
          let iceCandidate = msg.data as RTCIceCandidateInit;//JSON.parse(msg.data) as RTCIceCandidateInit;
          await ctx.localClient.addIceCandidate(iceCandidate);
          break;
        case "ping":
        let data = JSON.stringify({type: "pong", data: "pong"})
          ctx.socketService.send(data)
          break;
        default:
          console.log("Unknown message");
          console.log(msg);
      }
  }

  public setupRoomConnection(roomID: string,l: HTMLVideoElement, r: HTMLVideoElement){
    this.localVideo = l;
    this.remoteVideo = r;
    this.socketService.setContext(this);
  }


  private socketOpen(evt: Event){
    console.log("Websocket open")
    console.log(evt)
  }

  private socketClose(evt: CloseEvent){
    console.log("Websocket close")
    console.log(evt)
  }

  private socketError(evt: Event){
    console.log("Websocket error")
    console.log(evt)
  }

  public HangUp(msg: string){
    this.socketService.send(msg)
  }
}

class Command {
  constructor(cmd: string){this.type = cmd;}
  public type: string;
}