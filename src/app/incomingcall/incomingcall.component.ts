import { Component, OnInit, HostBinding, Input, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgTemplateOutlet } from '@angular/common';
import {Command} from '../commands';
import {SocketService} from 'src/app/socket.service';

@Component({
  selector: 'app-incomingcall',
  templateUrl: './incomingcall.component.html',
  styleUrls: ['./incomingcall.component.css']
})
export class IncomingcallComponent implements OnInit {

  constructor(private modalService: NgbModal,private socketService: SocketService) { }
  public roomID: string;

  @ViewChild("content") content: NgTemplateOutlet;

  ngOnInit() {
  }

  public async answer(){
    //route to call component
      let json = JSON.stringify({type: "answered", data: this.roomID})
      this.socketService.send(json)
    console.log(this.roomID);
    this.modalService.dismissAll("Answered")
  }
  
  public decline(){
    let msg = new Message()
    msg.Type = "declined"
    msg.Data = this.roomID
    msg.RoomID = this.roomID
    let json = JSON.stringify(msg)
    this.socketService.send(json)
  this.modalService.dismissAll("Declined")
  }

  public open(id: string){
    console.log("OPENED INCOMING CALL")
    this.modalService.open(this.content);
    this.roomID = id;
  }

  public close(){
    this.modalService.dismissAll("call already answered")
  }

}

class Message {
  Type: string;
  Data: any;
  RoomID: string;
}